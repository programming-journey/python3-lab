# coding: utf-8
import img2pdf
from PIL import Image
import os
img_path = "IMG.jpg"
pdf_path = "HW1.pdf"
image = Image.open(img_path)
pdf_bytes = img2pdf.convert(image.filename)
file = open(pdf_path,"wb")
file.write(pdf_bytes)
image.close()
file.close()
get_ipython().run_line_magic('save', 'image2pdf ~0/')

# coding: utf-8
import webview
import threading
import time

def change_url(window):
	time.sleep(10)
	window.load_url('https://pywebview.flowrl.com/hello')
if __name__ == '__main__':
    window = webview.create_window('URL Change Example', 'http://www.google.com',frameless=True)
    webview.start(change_url, window)
    webview.start(debug=True,gui='cef')
    
get_ipython().run_line_magic('save', 'hello ~0/')
